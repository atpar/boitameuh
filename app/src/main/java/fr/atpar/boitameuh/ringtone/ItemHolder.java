package fr.atpar.boitameuh.ringtone;

import java.util.ArrayList;
import java.util.List;

/**
 * Base class for wrapping an item for compatibility with an {@link ItemHolder}.
 * <p/>
 * An {@link ItemHolder} serves as bridge between the model and view layer; subclassers should
 * implement properties that fall beyond the scope of their model layer but are necessary for
 * the view layer.
 * <p/>
 * Note: An {@link ItemHolder} can be used by multiple {@link ItemHolder} and any state changes
 * should simultaneously be reflected in both UIs.  It is not thread-safe however and should
 * only be used on a single thread at a given time.
 *
 * @param <T> the item type wrapped by the holder
 */
abstract class ItemHolder<T> {

    /**
     * The item held by this holder.
     */
    private final T item;

    /**
     * Listeners to be invoked by {@link #notifyItemChanged()}.
     */
    private final List<OnItemChangedListener<T>> mOnItemChangedListeners = new ArrayList<>();

    /**
     * Designated constructor.
     *
     * @param item   the {@link T} item to be held by this holder
     */
    ItemHolder(final T item) {
        this.item = item;
    }


    T getItem() {
        return this.item;
    }

    /**
     * @return the unique identifier for the view that should be used to represent the item,
     * e.g. the layout resource id.
     */
    protected abstract int getItemViewType();

    /**
     * Adds the listener to the current list of registered listeners if it is not already
     * registered.
     *
     * @param listener the listener to add
     */
    final void addOnItemChangedListener(final OnItemChangedListener<T> listener) {
        if (!this.mOnItemChangedListeners.contains(listener)) {
            this.mOnItemChangedListeners.add(listener);
        }
    }

    /**
     * Removes the listener from the current list of registered listeners.
     *
     * @param listener the listener to remove
     */
    final void removeOnItemChangedListener(final OnItemChangedListener<T> listener) {
        this.mOnItemChangedListeners.remove(listener);
    }

    /**
     * Invokes {@link OnItemChangedListener#onItemChanged(ItemHolder)} for all listeners added
     * via {@link #addOnItemChangedListener(OnItemChangedListener)}.
     */
    final void notifyItemChanged() {
        for (final OnItemChangedListener<T> listener : this.mOnItemChangedListeners) {
            listener.onItemChanged(this);
        }
    }

}
