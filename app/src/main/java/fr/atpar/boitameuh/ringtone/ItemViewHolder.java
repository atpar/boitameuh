package fr.atpar.boitameuh.ringtone;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Base class for a reusable {@link RecyclerView.ViewHolder} compatible with an
 * {@link ItemViewHolder}. Provides an interface for binding to an {@link ItemHolder} and later
 * being recycled.
 */
class ItemViewHolder<T extends ItemHolder> extends RecyclerView.ViewHolder {

    /**
     * The current {@link ItemHolder} bound to this holder.
     */
    private T mItemHolder;

    /**
     * The current {@link OnItemClickedListener} associated with this holder.
     */
    private OnItemClickedListener<T> mOnItemClickedListener;

    /**
     * Designated constructor.
     *
     * @param itemView the item {@link View} to associate with this holder
     */
    ItemViewHolder(final View itemView) {
        super(itemView);
    }

    /**
     * @return the current {@link ItemHolder} bound to this holder, or {@code null} if unbound
     */
    final T getItemHolder() {
        return this.mItemHolder;
    }

    /**
     * Binds the holder's {@link #itemView} to a particular item.
     *
     * @param itemHolder the {@link ItemHolder} to bind
     */
    final void bindItemView(final T itemHolder) {
        this.mItemHolder = itemHolder;
        this.onBindItemView(itemHolder);
    }

    /**
     * Called when a new item is bound to the holder. Subclassers should override to bind any
     * relevant data to their {@link #itemView} in this method.
     *
     * @param itemHolder the {@link ItemHolder} to bind
     */
    void onBindItemView(final T itemHolder) {
        // for subclassers
    }

    /**
     * Recycles the current item view, unbinding the current item holder and state.
     */
    final void recycleItemView() {
        this.mItemHolder = null;
        this.mOnItemClickedListener = null;
    }


    /**
     * Sets the current {@link OnItemClickedListener} to be invoked via
     * {@link #notifyItemClicked}.
     *
     * @param listener the new {@link OnItemClickedListener}, or {@code null} to clear
     */
    final void setOnItemClickedListener(final OnItemClickedListener<T> listener) {
        this.mOnItemClickedListener = listener;
    }

    /**
     * Called by subclasses to invoke the current {@link OnItemClickedListener} for a
     * particular click event so it can be handled at a higher level.
     *
     * @param id the unique identifier for the click action that has occurred
     */
    final void notifyItemClicked(final int id) {
        if (null != this.mOnItemClickedListener) {
            this.mOnItemClickedListener.onItemClicked(this, id);
        }
    }

}
