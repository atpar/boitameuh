/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.atpar.boitameuh.ringtone;

import android.net.Uri;

import androidx.annotation.NonNull;

/**
 * A read-only domain object representing a custom ringtone chosen from the file system.
 */
final class CustomRingtone implements Comparable<CustomRingtone> {

    /** The unique identifier of the custom ringtone. */
    private final long mId;

    /** The uri that allows playback of the ringtone. */
    private final Uri mUri;

    /** The title describing the file at the given uri; typically the file name. */
    private final String mTitle;

    CustomRingtone(final long id, final Uri uri, final String title) {
        this.mId = id;
        this.mUri = uri;
        this.mTitle = title;
    }

    public long getId() { return this.mId; }
    public Uri getUri() { return this.mUri; }
    String getTitle() { return this.mTitle; }

    @Override
    public int compareTo(@NonNull final CustomRingtone other) {
        return String.CASE_INSENSITIVE_ORDER.compare(this.getTitle(), other.getTitle());
    }
}