package fr.atpar.boitameuh.ringtone;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.util.Log;

import fr.atpar.boitameuh.R;

import static android.content.Intent.FLAG_GRANT_READ_URI_PERMISSION;
import static android.provider.OpenableColumns.DISPLAY_NAME;

/**
 * This task locates a displayable string in the background that is fit for use as the title of
 * the audio content. It adds a custom ringtone using the uri and title on the main thread.
 */
@SuppressLint("StaticFieldLeak")
final class AddCustomRingtoneTask extends AsyncTask<Void, Void, String> {

    private final RingtonePickerActivity ringtonePickerActivity;
    private final Uri mUri;

    AddCustomRingtoneTask(final RingtonePickerActivity ringtonePickerActivityParam, final Uri uri) {
        this.ringtonePickerActivity = ringtonePickerActivityParam;
        this.mUri = uri;
    }

    @Override
    protected String doInBackground(final Void... voids) {
        String result = this.ringtonePickerActivity.getString(R.string.unknown_ringtone_title);
        final ContentResolver contentResolver = this.ringtonePickerActivity.getContentResolver();

        // Take the long-term permission to read (playback) the audio at the uri.
        contentResolver.takePersistableUriPermission(this.mUri, FLAG_GRANT_READ_URI_PERMISSION);

        try (final Cursor cursor = contentResolver.query(this.mUri, null, null, null, null)) {
            if (null != cursor && cursor.moveToFirst()) {
                // If the file was a media file, return its title.
                final int titleIndex = cursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
                if (-1 != titleIndex) {
                    result = cursor.getString(titleIndex);
                } else {
                    // If the file was a simple openable, return its display name.
                    final int displayNameIndex = cursor.getColumnIndex(DISPLAY_NAME);
                    if (-1 != displayNameIndex) {
                        String title = cursor.getString(displayNameIndex);
                        final int dotIndex = title.lastIndexOf(".");
                        if (0 < dotIndex) {
                            title = title.substring(0, dotIndex);
                        }
                        result = title;
                    }
                }
            } else {
                Log.e("AddCustomRingtone", "No ringtone for uri: " + this.mUri);
            }
        } catch (final Exception e) {
            Log.e("AddCustomRingtone", "Unable to locate title for custom ringtone: " + this.mUri, e);
        }

        return result;
    }

    @Override
    protected void onPostExecute(final String title) {
        this.ringtonePickerActivity.addCustomRingtone(this.mUri, title);
    }
}
