/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.atpar.boitameuh.ringtone;

import android.util.SparseArray;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import static androidx.recyclerview.widget.RecyclerView.NO_ID;

/**
 * Base adapter class for displaying a collection of items. Provides functionality for handling
 * changing items, persistent item state, item click events, and re-usable item views.
 */
@SuppressWarnings("unchecked")
class ItemAdapter<T extends ItemHolder>
        extends RecyclerView.Adapter<ItemViewHolder<T>> {

    /**
     * Factories for creating new {@link ItemViewHolder} entities.
     */
    private final SparseArray<ItemViewHolderFactory> mFactoriesByViewType = new SparseArray<>();
    /**
     * Listeners to invoke in {@link #mOnItemClickedListener}.
     */
    private final SparseArray<OnItemClickedListener<T>> mListenersByViewType = new SparseArray<>();
    /**
     * Invokes the {@link OnItemClickedListener} in {@link #mListenersByViewType} corresponding
     * to {@link ItemViewHolder#getItemViewType()}
     */
    private final OnItemClickedListener<T> mOnItemClickedListener = (viewHolder, id) -> {
        final OnItemClickedListener<T> listener =
                this.mListenersByViewType.get(viewHolder.getItemViewType());
        if (null != listener) {
            listener.onItemClicked(viewHolder, id);
        }
    };
    /**
     * List of current item holders represented by this adapter.
     */
    private List<T> mItemHolders;
    /**
     * Finds the position of the changed item holder and invokes {@link #notifyItemChanged(int)} or
     * {@link #notifyItemChanged(int, Object)} if payloads are present (in order to do in-place
     * change animations).
     */
    private final OnItemChangedListener<T> mItemChangedNotifier = itemHolder -> {
        @SuppressWarnings("SuspiciousMethodCalls") final int position = ItemAdapter.this.mItemHolders.indexOf(itemHolder);
        if (RecyclerView.NO_POSITION != position) {
            ItemAdapter.this.notifyItemChanged(position);
        }
    };

    /**
     * Sets the {@link ItemViewHolderFactory} and {@link OnItemClickedListener} used to create
     * new item view holders in {@link #onCreateViewHolder(ViewGroup, int)}.
     *
     * @param factory   the {@link ItemViewHolderFactory} used to create new item view holders
     * @param listener  the {@link OnItemClickedListener} to be invoked by
     *                  {@link #mItemChangedNotifier}
     * @param viewTypes the unique identifier for the view types to be created
     * @return this object, allowing calls to methods in this class to be chained
     */
    ItemAdapter withViewTypes(final ItemViewHolderFactory factory,
                              final OnItemClickedListener<ItemHolder> listener, final int... viewTypes) {
        for (final int viewType : viewTypes) {
            this.mFactoriesByViewType.put(viewType, factory);
            this.mListenersByViewType.put(viewType, (OnItemClickedListener<T>) listener);
        }
        return this;
    }

    /**
     * @return the current list of item holders represented by this adapter
     */
    final List<T> getItems() {
        return this.mItemHolders;
    }

    /**
     * Sets the list of item holders to serve as the dataset for this adapter and invokes
     * {@link #notifyDataSetChanged()} to update the UI.
     * <p/>
     *
     * @param itemHolders the new list of item holders
     */
    void setItems(final List<T> itemHolders) {
        final List<T> oldItemHolders = this.mItemHolders;
        if (oldItemHolders != itemHolders) {
            if (null != oldItemHolders) {
                // remove the item change listener from the old item holders
                for (final T oldItemHolder : oldItemHolders) {
                    oldItemHolder.removeOnItemChangedListener(this.mItemChangedNotifier);
                }
            }

            if (null != itemHolders) {
                // add the item change listener to the new item holders
                for (final ItemHolder newItemHolder : itemHolders) {
                    newItemHolder.addOnItemChangedListener(this.mItemChangedNotifier);
                }
            }

            // finally update the current list of item holders and inform the RV to update the UI
            this.mItemHolders = itemHolders;
            this.notifyDataSetChanged();
        }
    }

    /**
     * Removes the first occurrence of the specified element from this list, if it is present
     * (optional operation). If this list does not contain the element, it is unchanged. Invokes
     * {@link #notifyItemRemoved} to update the UI.
     *
     * @param itemHolder the item holder to remove
     */
    void removeItem(@NonNull T itemHolder) {
        final int index = this.mItemHolders.indexOf(itemHolder);
        if (0 <= index) {
            itemHolder = this.mItemHolders.remove(index);
            itemHolder.removeOnItemChangedListener(this.mItemChangedNotifier);
            this.notifyItemRemoved(index);
        }
    }

    @Override
    public int getItemCount() {
        return null == this.mItemHolders ? 0 : this.mItemHolders.size();
    }

    @Override
    public long getItemId(final int position) {
        return NO_ID;
    }

    @Override
    public int getItemViewType(final int position) {
        return this.mItemHolders.get(position).getItemViewType();
    }

    @Override
    @NonNull
    public ItemViewHolder<T> onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        final ItemViewHolderFactory<T> factory = this.mFactoriesByViewType.get(viewType);
        if (null != factory) {
            return factory.createViewHolder(parent, viewType);
        }
        throw new IllegalArgumentException("Unsupported view type: " + viewType);
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder<T> viewHolder, final int position) {
        // suppress any unchecked warnings since it is up to the subclass to guarantee
        // compatibility of their view holders with the item holder at the corresponding position
        viewHolder.bindItemView(this.mItemHolders.get(position));
        viewHolder.setOnItemClickedListener(this.mOnItemClickedListener);
    }

    @Override
    public void onViewRecycled(final ItemViewHolder<T> viewHolder) {
        viewHolder.setOnItemClickedListener(null);
        viewHolder.recycleItemView();
    }

}