package fr.atpar.boitameuh.ringtone;

/**
 * Callback interface for handling when an item is clicked.
 */
interface OnItemClickedListener<T extends ItemHolder> {
    /**
     * Invoked by {@link ItemViewHolder#notifyItemClicked(int)}
     *
     * @param viewHolder the {@link ItemViewHolder} containing the view that was clicked
     * @param id         the unique identifier for the click action that has occurred
     */
    void onItemClicked(ItemViewHolder<T> viewHolder, int id);
}
