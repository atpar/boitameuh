/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.atpar.boitameuh.ringtone;

import android.content.Context;
import android.net.Uri;

import androidx.loader.content.AsyncTaskLoader;

import java.util.ArrayList;
import java.util.List;

import fr.atpar.boitameuh.R;

/**
 * Assembles the list of ItemHolders that back the RecyclerView used to choose a ringtone.
 */
class RingtoneLoader extends AsyncTaskLoader<List<ItemHolder<Uri>>> {

    private final RingtoneModel ringtoneModel;
    private List<CustomRingtone> mCustomRingtones;

    RingtoneLoader(final Context context, final RingtoneModel ringtoneModelParam) {
        super(context);
        this.ringtoneModel = ringtoneModelParam;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();

        this.mCustomRingtones = this.ringtoneModel.getCustomRingtones();
        this.forceLoad();
    }

    @Override
    public List<ItemHolder<Uri>> loadInBackground() {
        // Prime the ringtone title cache for later access.
        this.ringtoneModel.loadRingtoneTitles();

        final List<ItemHolder<Uri>> itemHolders = new ArrayList<>();

        // Add the item holder for the Music heading.
        itemHolders.add(new HeaderHolder(R.string.your_sounds));

        // Add an item holder for each custom ringtone and also cache a pretty name.
        for (final CustomRingtone ringtone : this.mCustomRingtones) {
            itemHolders.add(new CustomRingtoneHolder(ringtone, this.ringtoneModel));
        }

        // Add an item holder for the "Add new" music ringtone.
        itemHolders.add(new AddCustomRingtoneHolder());

        // Add an item holder for the Ringtones heading.
        itemHolders.add(new HeaderHolder(R.string.embedded_sounds));

        // Add an item holder for every embedded ringtone.
        itemHolders.add(new SystemRingtoneHolder(Utils.getResourceUri(this.getContext(), R.raw.cat_0903_cut),
                this.getContext().getString(R.string.cat_sound_uri_title), this.ringtoneModel));
        itemHolders.add(new SystemRingtoneHolder(Utils.getResourceUri(this.getContext(), R.raw.hero_decorative_celebration_03),
                this.getContext().getString(R.string.celebration_sound_uri_title), this.ringtoneModel));
        itemHolders.add(new SystemRingtoneHolder(Utils.getResourceUri(this.getContext(), R.raw.meuglement_0546_cut),
                this.getContext().getString(R.string.cow_one_sound_uri_title), this.ringtoneModel));
        itemHolders.add(new SystemRingtoneHolder(Utils.getResourceUri(this.getContext(), R.raw.meuglement_0546_cut2),
                this.getContext().getString(R.string.cow_two_sound_uri_title), this.ringtoneModel));
        itemHolders.add(new SystemRingtoneHolder(Utils.getResourceUri(this.getContext(), R.raw.coq_0283),
                this.getContext().getString(R.string.rooster_sound_uri_title), this.ringtoneModel));

        return itemHolders;
    }

    @Override
    protected void onReset() {
        super.onReset();
        this.mCustomRingtones = null;
    }
}