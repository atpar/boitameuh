package fr.atpar.boitameuh.ringtone;

import android.content.Intent;
import android.net.Uri;

/**
 * This click handler alters selection and playback of ringtones. It also launches the system file chooser to search for openable audio files that may serve as
 * ringtones.
 */
class ItemClickWatcher implements OnItemClickedListener<ItemHolder<Uri>> {
    private final RingtonePickerActivity ringtonePickerActivity;

    ItemClickWatcher(final RingtonePickerActivity ringtonePickerActivity) {
        this.ringtonePickerActivity = ringtonePickerActivity;
    }

    @Override
    public void onItemClicked(final ItemViewHolder<ItemHolder<Uri>> viewHolder, final int id) {
        switch (id) {
            case AddCustomRingtoneViewHolder.CLICK_ADD_NEW:
                this.ringtonePickerActivity.stopPlayingRingtone(this.ringtonePickerActivity.getSelectedRingtoneHolder(), false);
                //noinspection HardcodedFileSeparator
                this.ringtonePickerActivity.startActivityForResult(new Intent(Intent.ACTION_OPEN_DOCUMENT)
                        .addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION)
                        .addCategory(Intent.CATEGORY_OPENABLE)
                        .setType("audio/*"), 0);
                break;

            case RingtoneViewHolder.CLICK_NORMAL:
                final RingtoneHolder oldSelection = this.ringtonePickerActivity.getSelectedRingtoneHolder();
                final RingtoneHolder newSelection = (RingtoneHolder) viewHolder.getItemHolder();

                // Tapping the existing selection toggles playback of the ringtone.
                if (oldSelection == newSelection) {
                    if (newSelection.isPlaying()) {
                        this.ringtonePickerActivity.stopPlayingRingtone(newSelection, false);
                    } else {
                        this.ringtonePickerActivity.startPlayingRingtone(newSelection);
                    }
                } else {
                    // Tapping a new selection changes the selection and playback.
                    this.ringtonePickerActivity.stopPlayingRingtone(oldSelection, true);
                    this.ringtonePickerActivity.startPlayingRingtone(newSelection);
                }
                break;

            case RingtoneViewHolder.CLICK_LONG_PRESS:
                this.ringtonePickerActivity.setIndexOfRingtoneToRemove(viewHolder.getAdapterPosition());
                break;
        }
    }
}
