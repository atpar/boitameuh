package fr.atpar.boitameuh.ringtone;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import fr.atpar.boitameuh.R;

public class AddCustomRingtoneViewHolderFactory implements ItemViewHolderFactory<AddCustomRingtoneHolder> {

    private final LayoutInflater mInflater;

    AddCustomRingtoneViewHolderFactory(final LayoutInflater inflater) {
        this.mInflater = inflater;
    }

    @NonNull
    @Override
    public ItemViewHolder<AddCustomRingtoneHolder> createViewHolder(@Nullable final ViewGroup parent, final int viewType) {
        final View itemView = this.mInflater.inflate(R.layout.ringtone_item_sound, parent, false);
        return new AddCustomRingtoneViewHolder(itemView);
    }
}
