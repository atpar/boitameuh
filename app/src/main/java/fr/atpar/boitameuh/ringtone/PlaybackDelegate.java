package fr.atpar.boitameuh.ringtone;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.net.Uri;

import androidx.annotation.NonNull;

/**
 * This interface abstracts away the differences between playing ringtones via {@link Ringtone}
 * vs {@link MediaPlayer}.
 */
interface PlaybackDelegate {
    /**
     * starts playing provided sound.
     */
    void play(@NonNull Context context, @NonNull Uri ringtoneUri);

    /**
     * Stop any ongoing ringtone playback.
     */
    void stop();
}
