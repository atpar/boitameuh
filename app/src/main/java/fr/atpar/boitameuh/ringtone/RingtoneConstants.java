package fr.atpar.boitameuh.ringtone;

/**
 * To manage shared constant values among ringtone elements.
 */
class RingtoneConstants {

    /**
     * Value to apply to ringtone items displayed in dedicated activity when a ringtone is not selected.
     */
    public static final float ALPHA_VALUE_DISABLED_RINGTONE = 0.63f;

    private RingtoneConstants() {
        // empty and private constructor for constants class.
    }
}
