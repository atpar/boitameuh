package fr.atpar.boitameuh.ringtone;

/**
 * Callback interface for when an item changes and should be re-bound.
 */
interface OnItemChangedListener<T> {
    /**
     * Invoked by {@link ItemHolder#notifyItemChanged()}.
     *
     * @param itemHolder the item holder that has changed
     */
    void onItemChanged(ItemHolder<T> itemHolder);

}
