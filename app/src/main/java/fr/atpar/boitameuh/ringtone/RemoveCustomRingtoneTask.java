package fr.atpar.boitameuh.ringtone;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import static android.content.Intent.FLAG_GRANT_READ_URI_PERMISSION;

/**
 * Removes a custom ringtone with the given uri. Taking this action has side-effects because
 * all alarms that use the custom ringtone are reassigned to the Android system default alarm
 * ringtone. If the application's default alarm ringtone is being removed, it is reset to the
 * Android system default alarm ringtone. If the application's timer ringtone is being removed,
 * it is reset to the application's default timer ringtone.
 */
@SuppressLint("StaticFieldLeak")
final class RemoveCustomRingtoneTask extends AsyncTask<Void, Void, Void> {

    private final Uri mRemoveUri;
    private final RingtonePickerActivity ringtonePickerActivity;

    RemoveCustomRingtoneTask(final RingtonePickerActivity ringtonePickerActivity, final Uri removeUri) {
        this.ringtonePickerActivity = ringtonePickerActivity;
        this.mRemoveUri = removeUri;
    }

    @Override
    protected Void doInBackground(final Void... voids) {
        // Update all alarms that use the custom ringtone to use the system default.
        final ContentResolver cr = this.ringtonePickerActivity.getContentResolver();

        try {
            // Release the permission to read (playback) the audio at the uri.
            cr.releasePersistableUriPermission(this.mRemoveUri, FLAG_GRANT_READ_URI_PERMISSION);
        } catch (final SecurityException exception) {
            // If the file was already deleted from the file system, a SecurityException is
            // thrown indicating this app did not hold the read permission being released.
            Log.w("RemoveCustomRingtone", "SecurityException while releasing read permission for " + this.mRemoveUri, exception);
        }

        return null;
    }

    @Override
    protected void onPostExecute(final Void v) {
        this.ringtonePickerActivity.removeCustomRingtone(this.mRemoveUri);
    }
}
