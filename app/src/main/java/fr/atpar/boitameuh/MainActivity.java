package fr.atpar.boitameuh;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioAttributes;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import java.io.FileNotFoundException;

import fr.atpar.boitameuh.ringtone.RingtoneModel;
import fr.atpar.boitameuh.ringtone.Utils;
import fr.atpar.boitameuh.settings.SettingsActivity;

public class MainActivity extends AppCompatActivity implements SensorEventListener {
    /**
     * Pattern to manage vibration. First element is vibration itself, and second element is repetition index.
     */
    private static final long[] VIBRATE_PATTERN = {500, 500};

    private SoundPool soundPool;
    private SensorManager sensorManager;
    private ImageView imageView;
    private long lastUpdate = System.nanoTime();
    private Uri customSoundUri;
    private long maximumTimeBetweenFlips;
    private boolean vibrationActivation;
    private int loadedSoundIdentifier;

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_main);

        // initialize our player with 5 streams to play simultaneously 5 times potentially the sound.
        final SoundPool.Builder soundPoolBuilder = new SoundPool.Builder();
        soundPoolBuilder.setMaxStreams(5);
        this.soundPool = soundPoolBuilder.build();

        // retrieve sensor manager (manage shaking)
        this.sensorManager = (SensorManager) this.getSystemService(Context.SENSOR_SERVICE);

        // retrieve image to shake
        this.imageView = this.findViewById(R.id.imageView);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // reset status
        this.lastUpdate = System.nanoTime();
        // update preferences.
        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this /* Activity context */);
        // delay between flips ...
        final long nanosecondsTranslationFactor = 100000000; // to convert preferences values to nanoseconds
        final int maximumTimeBetweenFlipsInDeciSeconds = sharedPreferences.getInt("maximumTimeBetweenFlips", 20);
        this.maximumTimeBetweenFlips = nanosecondsTranslationFactor * maximumTimeBetweenFlipsInDeciSeconds; // 2 second in nanoseconds
        // vibration activation
        this.vibrationActivation = sharedPreferences.getBoolean("vibrationActivation", false);
        // retrieve uri to play custom sound
        final RingtoneModel ringtoneModel = new RingtoneModel(this, sharedPreferences);
        this.customSoundUri = ringtoneModel.getTimerRingtoneUri();
        final int priority = 1; // Currently has no effect. Use a value of 1 for future compatibility.
        try {
            final AssetFileDescriptor assetFileDescriptor = this.getContentResolver().openAssetFileDescriptor(this.customSoundUri, "r");// "r" for read only
            this.loadedSoundIdentifier = this.soundPool.load(assetFileDescriptor, priority);
        } catch (final FileNotFoundException e) {
            Log.e("MainActivity", "Unable to locate selected uri: " + this.customSoundUri, e);
            // back to default
            this.loadedSoundIdentifier = this.soundPool.load(this, R.raw.meuglement_0546_cut2, priority);
        }

        // listen to shaking events ...
        this.sensorManager.registerListener(this, this.sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL, SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.sensorManager.unregisterListener(this);
    }

    /**
     * To force manually play of our sound.
     */
    public void onPlayButtonClick(@Nullable @SuppressWarnings("unused") final View view) {
        this.playAndAnimateCow();
    }

    /**
     * To navigate to settings activity.
     */
    public void onSettingsButtonClick(@Nullable @SuppressWarnings("unused") final View view) {
        // start settings activity
        final Intent intent = new Intent(this, SettingsActivity.class);
        this.startActivity(intent);
    }

    @Override
    public void onSensorChanged(@NonNull final SensorEvent sensorEvent) {
        /*
         * We would like to detect flip immediately followed by return (back flip).
         * Like a simulation of moo box shaking.
         */
        if (Sensor.TYPE_ACCELEROMETER == sensorEvent.sensor.getType()) {
            // Movement
            final float[] values = sensorEvent.values;
            final float x = values[0];
            final float y = values[1];
            final float z = values[2];

            final float accelerationSquareRoot = (x * x + y * y + z * z)
                    / (SensorManager.GRAVITY_EARTH * SensorManager.GRAVITY_EARTH);
            // The time in nanosecond at which the event happened
            final long actualTime = sensorEvent.timestamp;
            if (2 <= accelerationSquareRoot) { // arbitrary values to manage sensibility
                /*
                 * We have detected a movement. Yet we would like to have a double shake.
                 *
                 * First case, there was a long time between two updates: only one flip.
                 * Second case, there is a time between 200 milliseconds and 2 second: we have got a flip followed by a back flip, hurray !
                 * Third case, there is not enough time between two updates: calm down ...
                 */
                final long minimumTimeBetweenFlips = 200000000; // 200 milliseconds in nanoseconds
                final long interval = actualTime - this.lastUpdate;
                Log.d("boitameuh", "acceleration square root = " + accelerationSquareRoot);
                Log.d("boitameuh", "interval = " + interval);
                // update our detector
                this.lastUpdate = actualTime;

                // check status
                if (minimumTimeBetweenFlips < interval && interval < this.maximumTimeBetweenFlips) {
                    this.playAndAnimateCow();
                }
            }
        }
    }

    @Override
    public void onAccuracyChanged(@Nullable final Sensor sensor, final int accuracy) {
        // nothing to do here.
    }

    private void playAndAnimateCow() {
        // start playing
        this.soundPool.play(this.loadedSoundIdentifier, 1.0f, 1.0f, 1, 0, 1.0f);
        // launch animation of widget
        this.animatePicture();
        // vibrate ?
        if (this.vibrationActivation) {
            final Vibrator vibrator = this.getVibrator();
            if (Utils.isOOrLater()) {
                this.vibrateOOrLater(vibrator);
            } else if (Utils.isLOrLater()) {
                this.vibrateLOrLater(vibrator);
            } else {
                vibrator.vibrate(VIBRATE_PATTERN, -1);
            }
        }
    }

    private void animatePicture() {
        final ValueAnimator rotation = ObjectAnimator.ofPropertyValuesHolder(
                this.imageView, PropertyValuesHolder.ofFloat("rotation", 10f, -10f, 10f, -10f, 10f, -10f, 10f, -10f, 10f, -10f, 0f));
        final int animationDuration = 500;
        rotation.setDuration(animationDuration);
        rotation.start();
    }

    private Vibrator getVibrator() {
        return ((Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE));
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void vibrateLOrLater(final Vibrator vibrator) {
        vibrator.vibrate(VIBRATE_PATTERN, -1, new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_ALARM)
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .build());
    }

    @TargetApi(Build.VERSION_CODES.O)
    private void vibrateOOrLater(final Vibrator vibrator) {
        final VibrationEffect vibrationEffect = VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE);
        vibrator.vibrate(vibrationEffect);
    }
}
