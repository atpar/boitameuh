# This Dockerfile creates a static build image for CI

FROM openjdk:8-jdk

# Just matched `app/build.gradle`
ENV ANDROID_COMPILE_SDK "34"
# Just matched `app/build.gradle`
ENV ANDROID_BUILD_TOOLS "34.0.0"
# Version from https://developer.android.com/studio/releases/sdk-tools
ENV ANDROID_SDK_TOOLS "34.0.0"


# install OS packages
RUN apt-get --quiet update --yes
RUN apt-get --quiet install --yes wget apt-utils tar unzip lib32stdc++6 lib32z1 build-essential ruby ruby-dev
# We use this for xxd hex->binary
RUN apt-get --quiet install --yes vim-common

# install Android SDK
#rewrite using example from https://dev.to/fastphat/build-a-lightweight-docker-container-for-android-testing-ikh
RUN wget --quiet --output-document=android-sdk.zip https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip
RUN unzip -d android-sdk-linux -q android-sdk.zip
#update path
ENV ANDROID_SDK_ROOT /android-sdk-linux
ENV PATH ${PATH}:${ANDROID_SDK_ROOT}/tools:${ANDROID_SDK_ROOT}/tools/bin

#updates and install:
# android images and platform
# platform-tools
# sdk-tools
# build-tools
# emulator

#RUN yes y | sdkmanager --install "platform-tools" "system-images;android-${ANDROID_COMPILE_SDK};google_apis;x86_64" "platforms;android-${ANDROID_COMPILE_SDK}" "build-tools;${ANDROID_BUILD_TOOLS}" "emulator" >/dev/null
RUN yes y | sdkmanager --install "platform-tools" >/dev/null
RUN yes y | sdkmanager --install "system-images;android-${ANDROID_COMPILE_SDK};google_apis;x86_64" >/dev/null
RUN yes y | sdkmanager --install "platforms;android-${ANDROID_COMPILE_SDK}" >/dev/null
RUN yes y | sdkmanager --install "build-tools;${ANDROID_BUILD_TOOLS}" >/dev/null
RUN yes y | sdkmanager --install "emulator" >/dev/null

# accept license agreement for android sdk tools
# check licenses, even if it is too long for pipeline console
RUN yes y | sdkmanager --licenses
# second time, just to be sure ...
RUN yes y | sdkmanager --licenses

# install FastLane
COPY Gemfile.lock .
COPY Gemfile .
RUN gem install bundle
RUN bundle install
# update fastlane
RUN bundle update fastlane