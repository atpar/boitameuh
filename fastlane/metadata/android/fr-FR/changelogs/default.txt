Version initiale (12) :
* Belle image de vache !
* Déclenche l'animation lorsque vous secouez votre téléphone, ou appui sur le bouton "play"
* Permet la sélection d'un son utilisateur (disponible sur le téléphone)
    Par exemple, un enregistrement de votre voix.
* Animation de l'image de vache.
* Vibre lors de l'animation.
* Changement de lecteur pour jouer les sons lors de l'animation. Les performances sont au rendez-vous pour les sons courts, pas pour les sons longs (> 5s).